// https://leetcode.com/problems/reverse-integer/
function reverse() {
	let res = []
	for (let i = 0; i < this.length; i++) {
		const index = this.length - 1 - i
		res[i] = this[index]
	}
	return res
}

Array.prototype.reverse = reverse

console.log([1, 2, 3].reverse())