// https://leetcode.com/explore/learn/card/queue-stack/228/first-in-first-out-data-structure/1337/

/**
 * @param {number} k
 */
var MyCircularQueue = function (k) {
	this.maxSize = k
	this.array = []
};

/**
 * @param {number} value
 * @return {boolean}
 */
MyCircularQueue.prototype.enQueue = function (value) {
	if (this.isFull()) return false

	this.array.push(value)

	return true
};

/**
 * @return {boolean}
 */
MyCircularQueue.prototype.deQueue = function () {
	if (this.isEmpty()) return false

	this.array.shift()
	return true
};

/**
 * @return {number}
 */
MyCircularQueue.prototype.Front = function () {
	if (this.isEmpty()) return -1
	return this.array[0]
};

/**
 * @return {number}
 */
MyCircularQueue.prototype.Rear = function () {
	if (this.isEmpty()) return -1
	return this.array.slice(-1)[0]
};

/**
 * @return {boolean}
 */
MyCircularQueue.prototype.isEmpty = function () {
	return this.array.length === 0
};

/**
 * @return {boolean}
 */
MyCircularQueue.prototype.isFull = function () {
	return this.array.length === this.maxSize
};

/**
 * Your MyCircularQueue object will be instantiated and called as such:
 * var obj = new MyCircularQueue(k)
 * var param_1 = obj.enQueue(value)
 * var param_2 = obj.deQueue()
 * var param_3 = obj.Front()
 * var param_4 = obj.Rear()
 * var param_5 = obj.isEmpty()
 * var param_6 = obj.isFull()
 */


const values = [[3], [1], [2], [3], [4], [], [], [], [4], []];
let obj;
const arrayRes = ["MyCircularQueue", "enQueue", "enQueue", "enQueue", "enQueue", "Rear", "isFull", "deQueue", "enQueue", "Rear"]
	.map((func, valueIndex) => {
		const value = values[valueIndex][0]
		if (valueIndex === 0) {
			obj = new MyCircularQueue(value)
			return null
		}

		return obj[func](value)
	})

const expectedArray = [null, true, true, true, false, 3, true, true, true, 4]

const diff = arrayRes.map((value, index) => {
	return value === expectedArray[index]
})

console.log(JSON.stringify(arrayRes))
console.log(JSON.stringify(expectedArray))
console.log(JSON.stringify(diff))
