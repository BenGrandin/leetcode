// https://leetcode.com/problems/reverse-integer/

const x = -123
const HYPHEN = '-'
const CONSTRAINT = Math.pow(2, 31)

/**
 * @param {number} x
 * @return {number}
 */
const reverse = function(x) {
  let stringX = x.toString()
  let isNegative = false

  if (stringX[0] === HYPHEN) {
    isNegative = true
    stringX = stringX.replace(HYPHEN, '')
  }

  let res = parseInt([...stringX].reverse().join(''))

  if (res > CONSTRAINT) return 0
  return isNegative ? res * -1 : res

}

// console.log(reverse(123))
// console.log(reverse(-123))
console.log(reverse(90100))
// console.log(reverse(120))
console.log(reverse(102))
