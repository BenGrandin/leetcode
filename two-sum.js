// https://leetcode.com/problems/two-sum/

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
function twoSum(nums, target) {
	for (let i = 0; i < nums.length; i++) {
		const x = nums[i];
		for (let j = i + 1; j < nums.length; j++) {
			const y = nums[j];
			if (x + y === target) return [i, j];
		}
	}
}


// console.log(twoSum( [2, 7, 11, 15],9))
// console.log(twoSum([3, 2, 4], 6))
console.log(twoSum([3, 2, 3], 6))