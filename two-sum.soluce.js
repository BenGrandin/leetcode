// https://leetcode.com/problems/two-sum/
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
function twoSum(nums, target) {
	// TODO : How is that ?
	const hash = {}
	for (let i = 0; i < nums.length; i++) {
		const item = nums[i];
		if (hash[item]) {
			return [+hash[item], i]
		}
		hash[target - item] = i
	}
}


// console.log(twoSum([2, 7, 11, 15],9))
// console.log(twoSum([3, 2, 4], 6))
console.log(twoSum([3, 2, 3], 6))